# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['Purchase', 'PurchaseLine']


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'

    @fields.depends('office', 'company', 'lines')
    def on_change_office(self):
        if self.office:
            for line in self.lines:
                line.apply_analytic_rule()


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    def rule_analytic_pattern(self, template):
        pattern = super().rule_analytic_pattern(template)
        pattern['office'] = template.office.id if template.office else None
        return pattern
