# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.pyson import If, Eval


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.analytic_accounts.domain.extend([
            If(Eval('offices'),
                ('parent.offices', 'in', Eval('offices')), ()),
            (('parent.offices', 'in',
                Eval('context', {}).get('offices', [])))])
        if 'offices' not in cls.analytic_accounts.depends:
            cls.analytic_accounts.depends.append('offices')
