datalife_analytic_office
================================

The analytic_office module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-analytic_office/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-analytic_office)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
