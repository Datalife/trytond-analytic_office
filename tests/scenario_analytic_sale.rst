======================
Analytic Sale Scenario
======================

Imports::

    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company


Install analytic_office::

    >>> config = activate_modules(['analytic_office', 'analytic_party',
    ...     'analytic_sale', 'sale_office'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Get user::

    >>> User = Model.get('res.user')
    >>> admin = User(config.user)


Create Customer::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()


Create analytic accounts::

    >>> AnalyticAccount = Model.get('analytic_account.account')
    >>> root = AnalyticAccount()
    >>> root.name = 'Root'
    >>> root.code = '1'
    >>> root.type = 'root'
    >>> root.save()
    >>> view_account = AnalyticAccount()
    >>> view_account.name = 'View'
    >>> view_account.code = '11'
    >>> view_account.type = 'view'
    >>> view_account.root = root
    >>> view_account.parent = root
    >>> view_account.save()
    >>> view_account2 = AnalyticAccount()
    >>> view_account2.name = 'View 2'
    >>> view_account2.code = '12'
    >>> view_account2.type = 'view'
    >>> view_account2.root = root
    >>> view_account2.parent = root
    >>> view_account2.save()
    >>> analytic_account = AnalyticAccount()
    >>> analytic_account.name = 'Analytic 111'
    >>> analytic_account.code = '111'
    >>> analytic_account.root = root
    >>> analytic_account.parent = view_account
    >>> analytic_account.save()
    >>> analytic_account2 = AnalyticAccount()
    >>> analytic_account2.name = 'Analytic 121'
    >>> analytic_account2.code = '121'
    >>> analytic_account2.root = root
    >>> analytic_account2.parent = view_account2
    >>> analytic_account2.save()


Create office::

    >>> Office = Model.get('company.office')
    >>> office = Office()
    >>> office.name = 'Office 1'
    >>> office.company = company
    >>> office.save()
    >>> office2 = Office()
    >>> office2.name = 'Office 2'
    >>> office2.company = company
    >>> office2.save()


Setup offices to users and reload context::

    >>> admin.offices.append(office)
    >>> office = Office(office.id)
    >>> admin.offices.append(office2)
    >>> office2 = Office(office2.id)
    >>> admin.office = office
    >>> admin.save()
    >>> config._context = User.get_preferences(True, config.context)


Add analytic_accounts to offices::

    >>> office.analytic_accounts.append(view_account)
    >>> view_account = AnalyticAccount(view_account.id)
    >>> office.save()
    >>> office2.analytic_accounts.append(view_account2)
    >>> view_account2 = AnalyticAccount(view_account2.id)
    >>> office2.save()


Add anaytic accounts to customer::

    >>> customer.analytic_accounts.append(analytic_account)
    >>> analytic_account = AnalyticAccount(analytic_account.id)
    >>> customer.analytic_accounts.append(analytic_account2)
    >>> analytic_account2 = AnalyticAccount(analytic_account2.id)
    >>> customer.save()


Create Product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.list_price = Decimal('40')
    >>> template.salable = True
    >>> template.save()
    >>> product, = template.products


Create Sale::

    >>> Sale = Model.get('sale.sale')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.office = office
    >>> sline = sale.lines.new()
    >>> sline.product = product
    >>> sline.quantity = 2.0
    >>> entry_acc, = sline.analytic_accounts
    >>> entry_acc.account == analytic_account
    True
    >>> sale.save()


Create sale with office2::

    >>> sale2 = Sale()
    >>> sale2.party = customer
    >>> sale2.office = office2
    >>> sline = sale2.lines.new()
    >>> sline.product = product
    >>> sline.quantity = 2.0
    >>> entry_acc, = sline.analytic_accounts
    >>> entry_acc.account == analytic_account2
    True
    >>> sale2.save()