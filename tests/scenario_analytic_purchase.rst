==========================
Analytic Purchase Scenario
==========================

Imports::

    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company


Install analytic_party::

    >>> config = activate_modules(['analytic_office', 'analytic_party',
    ...     'analytic_purchase', 'purchase_office'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Get user::

    >>> User = Model.get('res.user')
    >>> admin = User(config.user)


Create Supplier::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()


Create analytic accounts::

    >>> AnalyticAccount = Model.get('analytic_account.account')
    >>> root = AnalyticAccount()
    >>> root.name = 'Root'
    >>> root.code = '1'
    >>> root.type = 'root'
    >>> root.save()
    >>> view_account = AnalyticAccount()
    >>> view_account.name = 'View'
    >>> view_account.code = '11'
    >>> view_account.type = 'view'
    >>> view_account.root = root
    >>> view_account.parent = root
    >>> view_account.save()
    >>> view_account2 = AnalyticAccount()
    >>> view_account2.name = 'View 2'
    >>> view_account2.code = '12'
    >>> view_account2.type = 'view'
    >>> view_account2.root = root
    >>> view_account2.parent = root
    >>> view_account2.save()
    >>> analytic_account = AnalyticAccount()
    >>> analytic_account.name = 'Analytic 111'
    >>> analytic_account.code = '111'
    >>> analytic_account.root = root
    >>> analytic_account.parent = view_account
    >>> analytic_account.save()
    >>> analytic_account2 = AnalyticAccount()
    >>> analytic_account2.name = 'Analytic 121'
    >>> analytic_account2.code = '121'
    >>> analytic_account2.root = root
    >>> analytic_account2.parent = view_account2
    >>> analytic_account2.save()


Create office::

    >>> Office = Model.get('company.office')
    >>> office = Office()
    >>> office.name = 'Office 1'
    >>> office.company = company
    >>> office.save()
    >>> office2 = Office()
    >>> office2.name = 'Office 2'
    >>> office2.company = company
    >>> office2.save()


Setup offices to users and reload context::

    >>> admin.offices.append(office)
    >>> office = Office(office.id)
    >>> admin.offices.append(office2)
    >>> office2 = Office(office2.id)
    >>> admin.office = office
    >>> admin.save()
    >>> config._context = User.get_preferences(True, config.context)


Add analytic_accounts to offices::

    >>> office.analytic_accounts.append(view_account)
    >>> view_account = AnalyticAccount(view_account.id)
    >>> office.save()
    >>> office2.analytic_accounts.append(view_account2)
    >>> view_account2 = AnalyticAccount(view_account2.id)
    >>> office2.save()


Add anaytic accounts to supplier::

    >>> supplier.analytic_accounts.append(analytic_account)
    >>> analytic_account = AnalyticAccount(analytic_account.id)
    >>> supplier.analytic_accounts.append(analytic_account2)
    >>> analytic_account2 = AnalyticAccount(analytic_account2.id)
    >>> supplier.save()


Create Product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.list_price = Decimal('40')
    >>> template.purchasable = True
    >>> template.save()
    >>> product, = template.products


Create Purchase::

    >>> Purchase = Model.get('purchase.purchase')
    >>> purchase = Purchase()
    >>> purchase.party = supplier
    >>> purchase.office = office
    >>> pline = purchase.lines.new()
    >>> pline.product = product
    >>> pline.quantity = 2.0
    >>> entry_acc, = pline.analytic_accounts
    >>> entry_acc.account == analytic_account
    True
    >>> purchase.save()


Create purchase with office2::

    >>> purchase2 = Purchase()
    >>> purchase2.party = supplier
    >>> purchase2.office = office2
    >>> pline = purchase2.lines.new()
    >>> pline.product = product
    >>> pline.quantity = 2.0
    >>> entry_acc, = pline.analytic_accounts
    >>> entry_acc.account == analytic_account2
    True
    >>> purchase2.save()