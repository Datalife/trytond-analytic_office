# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['Rule']


class Rule(metaclass=PoolMeta):
    __name__ = 'analytic_account.rule'

    office = fields.Many2One('company.office', 'Office',
        domain=['OR',
            ('company', '=', None),
            ('company', '=', Eval('company'))
        ],
        depends=['company'])


class AnalyticAccount(metaclass=PoolMeta):

    __name__ = 'analytic_account.account'

    offices = fields.Many2Many(
        'company.office-analytic_account.account', 'account', 'office',
        'Offices',
        domain=[
            ('company', '=', Eval('context', {}).get('company', -1))])
